﻿
using ColonialistsConsole;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace Colonialists.ViewModels
{
    public class EdgeViewModel
    {
        public VertexViewModel[] Vertices { get; set; }

        public Edge ToModel()
        {
            return new Edge(Vertices[0].ToModel(), Vertices[1].ToModel());
        }

    }
}