﻿using ColonialistsConsole;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace Colonialists.ViewModels
{
    public class CoordinatesViewModel
    {
        /// <summary>
        /// Радиус
        /// </summary>
        public double Radius { get;  set; }

        /// <summary>
        /// Угол
        /// </summary>
        public double Angle { get;  set; }

        public Coordinates ToModel()
        {
            return new Coordinates(Radius, Angle);
        }
    }
}