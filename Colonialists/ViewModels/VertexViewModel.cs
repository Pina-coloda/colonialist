﻿using ColonialistsConsole;

namespace Colonialists.ViewModels
{
    public class VertexViewModel
    {
        /// <summary>
        /// Координаты
        /// </summary>
        public CoordinatesViewModel Coordinates { get; set; }

        public Vertex ToModel()
        {
            return new Vertex(Coordinates.ToModel());
        }
    }
}