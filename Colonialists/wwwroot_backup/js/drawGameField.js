function init() {
    //Настройки элементов
    const canvas = document.getElementById('hexagonCanvas');
    const canvasWidth = 700;
    const canvasHeight = 700;
    const beginAngle = -90;
    const scale = 70;
    const imgWidth = 205;
    const imgHeight = imgWidth / 1.333;
    const vertexRadius = 15;
    const vertexColor = 'moccasin';
    const edgeWidth = 10;
    const edgeColor = 'moccasin';
    const circleHexRadius = 20;
    const circleHexColor = 'moccasin';
    const circleHexFontSize = 20;
    const circleHexFont = 'Arial';
    const circleHexFontColor = '#222';
    const circleHexBorderColor = '#222';
    const circleHexBorderWidth = 2;

    canvas.width = canvasWidth;
    canvas.height = canvasHeight;

    //Главный контейнер кавас
    let stage = new createjs.Stage("hexagonCanvas");
    stage.x = canvasWidth/2;
    stage.y = canvasHeight/2;
    stage.rotation = beginAngle;

    //Определение координат  X и Y
    const getX = (radius, angle) => radius * scale * Math.cos(angle * (-1) * Math.PI / 180);
    const getY = (radius, angle) => radius * scale * Math.sin(angle * (-1) * Math.PI / 180);

    //Отрисовка гексов с изображением
    for (let i = 0; i <elements.hexes.length; i++) {
        let bitmap = new createjs.Bitmap('images/' + elements.hexes[i].resourceType + '.png');
        bitmap.scaleX = imgWidth / bitmap.image.width;    
        bitmap.scaleY = imgHeight / bitmap.image.height;
        bitmap.rotation = beginAngle * (-1);
        stage.addChild(bitmap);
        let xBit = getX(elements.hexes[i].coordinates.radius, elements.hexes[i].coordinates.angle);
        let yBit = getY(elements.hexes[i].coordinates.radius, elements.hexes[i].coordinates.angle);
        bitmap.x = xBit + (imgHeight/2);
        bitmap.y = yBit - (imgWidth/2);

        let hex = new createjs.Shape();
        hex.graphics.moveTo(getX(elements.hexes[i].vertices[0].radius, elements.hexes[i].vertices[0].angle), getY(elements.hexes[i].vertices[0].radius, elements.hexes[i].vertices[0].angle));
        for (let j = 1; j < 6; j++) {
            hex.graphics.lineTo(getX(elements.hexes[i].vertices[j].radius, elements.hexes[i].vertices[j].angle), 
            getY(elements.hexes[i].vertices[j].radius, elements.hexes[i].vertices[j].angle));
        }
        hex.graphics.closePath();
        bitmap.mask = hex;
        stage.addChild(hex);
    }

    //Отрисовка линий
    for (let i = 0; i < elements.edges.length; i++) {
        let edge = new createjs.Shape();
        edge.graphics.setStrokeStyle(edgeWidth);
        edge.graphics.beginStroke(edgeColor);
        edge.graphics.moveTo(getX(elements.edges[i].from.radius, elements.edges[i].from.angle), getY(elements.edges[i].from.radius, elements.edges[i].from.angle));
        edge.graphics.lineTo(getX(elements.edges[i].to.radius, elements.edges[i].to.angle), getY(elements.edges[i].to.radius, elements.edges[i].to.angle));
        edge.graphics.endStroke();
        edge.radiusFrom = elements.edges[i].from.radius;
        edge.angleFrom = elements.edges[i].from.angle;
        edge.radiusTo = elements.edges[i].to.radius;
        edge.angleTo = elements.edges[i].to.angle;
        edge.addEventListener('click', function () {
            fetch('http://localhost:49685/api/game/RoadBuild/' + localStorage.gameGuid + '/jozhik', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'},
                body: JSON.stringify({
                        vertices: [
                            {coordinates: {
                                radius: edge.radiusFrom,
                                angle: edge.angleFrom
                            }}, 
                            {coordinates: {
                                radius: edge.radiusTo,
                                angle: edge.angleTo
                            }}
                        ]
                })
            })
                .then(response => console.log(response.json()))
                .then(result => console.log("работает"));
            document.querySelector('.radiusFrom').innerHTML = edge.radiusFrom;
            document.querySelector('.angleFrom').innerHTML = edge.angleFrom;
            document.querySelector('.radiusTo').innerHTML = edge.radiusTo;
            document.querySelector('.angleTo').innerHTML = edge.angleTo;
        })
        stage.addChild(edge);
    }

    //Отрисовка кругов с номерами
    for(let i = 0; i < elements.hexes.length; i++) {
        let numberHex = new createjs.Container();
        numberHex.x = getX(elements.hexes[i].coordinates.radius, elements.hexes[i].coordinates.angle);
        numberHex.y = getY(elements.hexes[i].coordinates.radius, elements.hexes[i].coordinates.angle);
        numberHex.rotation = beginAngle * (-1);
        let circle = new createjs.Shape();
        circle.graphics.beginFill(circleHexColor).beginStroke(circleHexBorderColor).setStrokeStyle(circleHexBorderWidth).drawCircle(0, 0, circleHexRadius);

        let number = new createjs.Text();
        number.set({
            text: elements.hexes[i].number,
            textAlign: "center",
            textBaseline: "middle",
            font: circleHexFontSize + 'px ' + circleHexFont,
            color: circleHexFontColor
          });
        numberHex.addChild(circle, number);
        stage.addChild(numberHex);
    }

    //Отрисовка вертексов
    for (let i = 0; i < elements.vertices.length; i++) {
        let vertex = new createjs.Shape();
        vertex.graphics.beginFill(vertexColor).drawCircle(0, 0, vertexRadius);
        vertex.x = getX(elements.vertices[i].radius, elements.vertices[i].angle);
        vertex.y = getY(elements.vertices[i].radius, elements.vertices[i].angle);
        vertex.radius = String(elements.vertices[i].radius);
        vertex.angle = String(elements.vertices[i].angle);
        vertex.addEventListener('click', function () {
            document.querySelector('.radius').innerHTML = vertex.radius;
            document.querySelector('.angle').innerHTML = vertex.angle;
        })
        stage.addChild(vertex);
    }

    stage.update();
}