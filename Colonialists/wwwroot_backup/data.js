//Главный объект с данными об игровом поле
const elements = {
    vertices: [],
    edges: [],
    hexes: []
}

//Массив игроков - временный
const players = JSON.stringify([{
    name: 'dimanoid'
}, {
    name: 'jozhik'
}, {
    name: 'belka'
}]
);

//Получение идентификатора игры
fetch('http://localhost:49685/api/game/gameGuid', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json;charset=utf-8'},
    body: players
})
    .then(response => response.json())
    .then(result => {localStorage.gameGuid = result});

//Получения списка гексов
fetch('http://localhost:49685/api/game/GetHexes?gameId=' + localStorage.gameGuid)
    .then(response => response.json())
    .then(result => {generateHexes(result); init();});


//Создание вертексов
for (let i = 0; i < 6; i++) {
    elements.vertices.push({
        radius: 1,
        angle: 60 * i,
    });
    elements.vertices.push({
        radius: 2,
        angle: 60 * i
    });
    elements.vertices.push({
        radius: 4,
        angle: 60 * i
    });
}

function createVertices(r, j) {
    for (let i = 0; i < j.length; i++) {
        elements.vertices.push({
            radius: r,
            angle: Math.asin(Math.sqrt(3) / r * j[i] / 2) * 180 / Math.PI
        });
        elements.vertices.push({
            radius: r,
            angle: 360 - Math.asin(Math.sqrt(3) / r * j[i] / 2) * 180 / Math.PI
        });
        elements.vertices.push({
            radius: r,
            angle: 180 - Math.asin(Math.sqrt(3) / r * j[i] / 2) * 180 / Math.PI
        });
        elements.vertices.push({
            radius: r,
            angle: 180 + Math.asin(Math.sqrt(3) / r * j[i] / 2) * 180 / Math.PI
        });
    }
}

createVertices(Math.sqrt(7), [1, 2, 3]);
createVertices(Math.sqrt(13), [1, 3, 4]);
createVertices(Math.sqrt(19), [2, 3, 5]);

//Расчет дистанции между двумя вертексами
function distance(coordinate1, coordinate2) {
    return Math.sqrt(coordinate1.radius * coordinate1.radius + coordinate2.radius * coordinate2.radius - 2 * coordinate1.radius * coordinate2.radius * Math.cos((coordinate1.angle - coordinate2.angle) * Math.PI / 180));
}

//Создание линий
elements.vertices.forEach(vertex1 => elements.vertices.filter(vertex2 => Math.abs(1 - distance(vertex1, vertex2)) < 1e-6).forEach(vertex => {
    if (elements.edges.some(x => x.from === vertex1 && x.from === vertex && x.to === vertex1 && x.to === vertex)) return;
    elements.edges.push({
        from: vertex1,
        to: vertex
    })
}));

//Создание гексов
function generateHexes (listHexes) {
    listHexes.forEach(x => elements.hexes.push(x));
    elements.hexes.forEach(hex => hex.vertices =
        elements.vertices.filter(vertex => Math.abs(1 - distance(vertex, hex.coordinates)) < 1e-6)
    );
    elements.hexes.forEach(hex => hex.vertices.sort((a, b) => newCoord(a, hex.coordinates) - newCoord(b, hex.coordinates)));
}

//Определение угла вертекса в гексе относительно его центра
function newCoord(vertex, centerVertex) {
    let x = (vertex.radius * Math.cos(vertex.angle * (-1) * Math.PI / 180)) - (centerVertex.radius * Math.cos(centerVertex.angle * (-1) * Math.PI / 180));
    let y = (vertex.radius * Math.sin(vertex.angle * (-1) * Math.PI / 180)) - (centerVertex.radius * Math.sin(centerVertex.angle * (-1) * Math.PI / 180));
    let angle = Math.atan2(y , x);
    return angle;
}
