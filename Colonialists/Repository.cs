﻿using System;
using System.Collections.Generic;
using System.Linq;
using ColonialistsConsole;
using LanguageExt;

namespace Colonialists
{
    public class Repository<T> where T: class, IModel
    {
        private readonly List<T> _list = new List<T>();

        /// <summary>
        /// Добавить в список
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            _list.Add(item);
        }

        /// <summary>
        /// Удалить из списка
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Delete(T item)
        {
            return _list.Remove(item);
        }

        /// <summary>
        /// Удалить из списка по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Delete(Guid id)
        {
            var item = _list.FirstOrDefault(x => x.Id == id);
            if (item == null)
            {
                return false;
            }

            return Delete(item);
        }

        /// <summary>
        /// Получить по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Option<T> Get(Guid id)
        {
            return _list.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Получить весь список
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> Get()
        {
            return _list;
        }
    }
}