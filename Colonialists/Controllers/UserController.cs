﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Colonialists.ViewModels;
using ColonialistsConsole;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Colonialists.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private Context _context { get; }

        [HttpPost("Add")]
        public ActionResult Add([FromBody] UserRegViewModel user)
        {

            if (_context.Users.Get().Any(x => x.Login == user.Login))
                return BadRequest("Такой пользователь уже существует");

            var userAdd = new User {Login = user.Login, Password = user.Password};
            _context.Users.Add(userAdd);
            return Ok(userAdd.Id);
                
        }

        public UserController(Context context)
        {
            _context = context;
        }
    }
}