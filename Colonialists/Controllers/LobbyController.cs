﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ColonialistsConsole;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace Colonialists.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LobbyController : ControllerBase
    {

        private Context _context { get; }
        private readonly IHubContext<PushHub<Lobby>> _hubContext;

        [Authorize]
        [HttpPost("create")]
        public ActionResult Create(string name)
        {
            return _context.Users.Get(Guid.Parse(User.FindFirst("Id").Value)).Some( (user) =>
            {
                var lobby = new Lobby {Name = name};
                lobby.Users.Add(user);
                _context.Lobby.Add(lobby);
                var lobbyOut = new
                {
                    Id = lobby.Id,
                    Name = lobby.Name,
                    Users = lobby.Users.Select(u => new {u.Id, Name = u.Login}).ToList()
                };
                 _hubContext.Clients.All.SendAsync("Add", lobbyOut).Wait();
                return (ActionResult) Ok(lobbyOut);
            }).None(() => NotFound("игрок не найден"));
        }

        [Authorize]
        [HttpPost("adduser")]
        public ActionResult AddUser(Guid lobbyId)
        {
            return _context.Lobby.Get(lobbyId).Some(lobby =>
            
               _context.Users.Get(Guid.Parse(User.FindFirst("Id").Value)).Some(user =>
                {
                    lobby.AddUser(user);
                    _hubContext.Clients.All.SendAsync("AddUser", new
                    {
                        Id = lobby.Id,
                        UserAdd = new { user.Id, Name = user.Login}
                    }).Wait();
                    return (ActionResult) Ok();
                }).None(() => NotFound("Игрок не найден"))
            ).None(() => NotFound("Лобби не найдено"));
        }

        [HttpPost]
        public ActionResult DeleteUser(Guid lobbyId, Guid userId)
        {
            return _context.Lobby.Get(lobbyId).Some(lobby =>
                {
                    if (lobby.DeleteUser(userId))
                        return (ActionResult) Ok();
                    return NotFound("Игрок не найден");
                }
            ).None(() => NotFound("Лобби не найдено"));
        }

        [HttpPost("creategame/{lobbyId}")]
        public ActionResult<Game> CreateGame(Guid lobbyId)
        {
            return _context.Lobby.Get(lobbyId)
                .Some(lobby => lobby.CreateGame()
                    .Some(game => {
                        _context.Games.Add(game);
                        return (ActionResult) Ok(game.Id);
                        })
                    .None(() => BadRequest("Неправильное количество игроков")))
                .None(() => NotFound("Лобби не найдено"));
        }

        [HttpGet("{lobbyId}")]
        public ActionResult Get(Guid lobbyId)
        {
            return _context.Lobby.Get(lobbyId)
                .Some(lobby =>
                {
                    var lobbyOut = new
                    {
                        Id = lobby.Id,
                        Name = lobby.Name,
                        Users = lobby.Users.Select(u => new {u.Id, Name = u.Login}).ToList()
                    };
                    return (ActionResult) Ok(lobbyOut);
                })
                .None(() => NotFound("Лобби не найдено"));
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(_context.Lobby.Get().Select(x => new
            {
                x.Id, x.Name, Users = x.Users.Select(u => new { u.Id, Name = u.Login }).ToList() })
                .ToList());
        }

        public LobbyController(Context context, IHubContext<PushHub<Lobby>> hubContext)
        {
            _context = context;
            _hubContext = hubContext;
        }
    }
}