﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Colonialists.ViewModels;
using ColonialistsConsole;
using LanguageExt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace Colonialists.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private Context _context { get; }

        [HttpPost("token")]
        public ActionResult Token([FromBody] UserRegViewModel user)
        {
            var identity = GetIdentity(user.Login, user.Password);
            if (identity == null)
            {
                return BadRequest(new { errorText = "Неправильный логин или пароль" });
            }

            var encodedJwt = GenerateToken(identity.Claims);
            var refreshToken = AuthOptions.GenerateRefreshToken();
            _context.Users.Get().FirstOrDefault(x => x.Login == identity.Name).RefreshToken = refreshToken;
 
            var response = new
            {
                access_token = encodedJwt,
                refresh_token = refreshToken,
                username = identity.Name
            };
 
            return Ok(response);
        }

        [HttpPost("refresh")]
        [Consumes("application/x-www-form-urlencoded")]
        public IActionResult Refresh([FromForm] string token, [FromForm] string refreshToken)
        {
            var principal = GetPrincipalFromExpiredToken(token);
            var user = _context.Users.Get().FirstOrDefault(x => x.Login == principal.Identity.Name);
            var savedRefreshToken =user.RefreshToken; //retrieve the refresh token from a data store
            if (savedRefreshToken != refreshToken)
                throw new SecurityTokenException("Invalid refresh token");

            var newJwtToken = GenerateToken(principal.Claims);
            var newRefreshToken = AuthOptions.GenerateRefreshToken();
            user.RefreshToken = newRefreshToken;

            return new ObjectResult(new {
                token = newJwtToken,
                refreshToken = newRefreshToken
            });
        }

        private string GenerateToken(IEnumerable<Claim> claims)
        {
            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }
 
        private ClaimsIdentity GetIdentity(string login, string password)
        {
            User user = _context.Users.Get().FirstOrDefault(x => x.Login == login && x.Password == password);
            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim("Id", user.Id.ToString()),
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login)
                };
                ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                return claimsIdentity;
            }
 
            // если пользователя не найдено
            return null;
        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AuthOptions.KEY)),
                ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }

        public AccountController(Context context)
        {
            _context = context;
        }
    }
}