﻿using System;
using System.Collections.Generic;
using System.Linq;
using Colonialists.ViewModels;
using ColonialistsConsole;
using Microsoft.AspNetCore.Mvc;
using LanguageExt;

namespace Colonialists.Controllers
{
    [Produces("application/json")]
    [Route("api/game")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private Context _context { get; }

        [HttpPost]
        [Route("RoadBuild/{gameId}/{playerName}")]
        public ActionResult RoadBuild(Guid gameId, string playerName, [FromBody] EdgeViewModel edge)
        {
            var game = _context.Games.Get(gameId);
            return game.Some(g =>
            {
                var edgeB = g.GameField.Edges.FirstOrDefault(x => x.Equals(edge.ToModel()));
                var player = g.Players.FirstOrDefault(x => x.Name == playerName);
                var result = player.BuildRoad(edgeB);
                return (ActionResult) Ok(result);

            }).None(() => NotFound("Игра не найдена"));
        }

        [HttpGet]
        [Route("GetHexes")]
        public ActionResult<IEnumerable<Hex>> GetHexes(Guid gameId)
        {
            return _context.Games.Get(gameId).Some(g => (ActionResult) Ok(g.GameField.Hexes))
                .None(() => NotFound($"Не найдена игра с GUID {gameId}"));
        }

        public GameController(Context context)
        {
            _context = context;
        }
    }
}
