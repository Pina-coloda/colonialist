import { User } from './User';

export class Lobby {
    public id: String = '';
    public name: String = '';
    public users: Array<User> = [];
}
