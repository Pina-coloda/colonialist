import { Vertex } from './Vertex';

export class Edge {
    public from: Vertex = new Vertex(0, 0);
    public to: Vertex = new Vertex(0, 0);

    constructor(vertex1: Vertex, vertex2: Vertex) {
        this.from = vertex1;
        this.to = vertex2;
    }
}
