import { Vertex } from './Vertex';
export class Edge {
    constructor(vertex1, vertex2) {
        this.from = new Vertex(0, 0);
        this.to = new Vertex(0, 0);
        this.from = vertex1;
        this.to = vertex2;
    }
}
//# sourceMappingURL=Edge.js.map