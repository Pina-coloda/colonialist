import { Coordinates } from '@/components/game/Coordinates';

export class Vertex {
    public coordinates: Coordinates = { radius: 0, angle: 0 };
    constructor(radius: number, angle: number) {
        this.coordinates = { radius: radius, angle: angle };
    }
}
