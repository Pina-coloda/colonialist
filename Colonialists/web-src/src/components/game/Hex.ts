import { Vertex } from './Vertex';
import { Coordinates } from './Coordinates';

export class Hex {
    public coordinates: Coordinates = {
        radius: 0,
        angle: 0
    };
    public resourceType: number | null = 0;
    public number: number = 0;

    constructor(resourceType: number | null, number: number, coordinates: Coordinates) {
        this.resourceType = resourceType;
        this.number = number;
        this.coordinates = coordinates;
    }
}
