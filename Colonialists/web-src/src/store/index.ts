import Vue from 'vue';
import Vuex from 'vuex';
import { Vertex } from '@/components/game/Vertex';
import { Edge } from '@/components/game/Edge';
import { Hex } from '@/components/game/Hex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: {
            name: 'Личный кабинет',
            accessToken: '',
            refreshToken: ''
        },
        gameField: {
            vertices: Array<Vertex>(),
            edges: Array<Edge>(),
            hexes: Array<Hex>()
        }
    },
    mutations: {
        login(state, user) {
            state.user = user;
        },
        addVertex(state, vertex: Vertex) {
            state.gameField.vertices.push(vertex);
        },
        addEdge(state, edge) {
            state.gameField.edges.push(edge);
        },
        addHex(state, hex) {
            state.gameField.hexes.push(hex);
        }
    },
    actions: {},
    modules: {},
    getters: {
        isAutorization(state): boolean {
            return state.user.accessToken != '';
        }
    }
});
