import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        user: {
            name: 'Личный кабинет',
            accessToken: '',
            refreshToken: ''
        },
        gameField: {
            vertices: Array(),
            edges: Array(),
            hexes: Array()
        }
    },
    mutations: {
        login(state, user) {
            state.user = user;
        },
        addVertex(state, vertex) {
            state.gameField.vertices.push(vertex);
        },
        addEdge(state, edge) {
            state.gameField.edges.push(edge);
        },
        addHex(state, hex) {
            state.gameField.hexes.push(hex);
        }
    },
    actions: {},
    modules: {},
    getters: {
        isAutorization(state) {
            return state.user.accessToken != '';
        }
    }
});
//# sourceMappingURL=index.js.map