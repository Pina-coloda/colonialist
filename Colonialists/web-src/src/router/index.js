import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
Vue.use(VueRouter);
const routes = [
    {
        path: '/auterization',
        name: 'auterization',
        component: () => import('@/components/authentication/Autorization.vue')
    },
    {
        path: '/registration',
        name: 'registration',
        component: () => import('@/components/authentication/Registration.vue')
    },
    {
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
        path: '/lobby-list',
        name: 'lobby-list',
        component: () => import('@/views/lobby/LobbyList.vue')
    },
    {
        path: '/lobby-item',
        name: 'lobby-item',
        props: true,
        component: () => import('@/views/lobby/LobbyItem.vue')
    },
    {
        path: '/game',
        name: 'game',
        props: true,
        component: () => import('@/views/Game.vue')
    },
    {
        path: '/lobby-create',
        name: 'lobby-create',
        component: () => import('@/views/lobby/LobbyCreate.vue')
    }
];
const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});
export default router;
//# sourceMappingURL=index.js.map