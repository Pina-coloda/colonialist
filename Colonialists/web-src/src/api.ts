export async function fetchWithCredentials(url: any, options?: any): Promise<any> {
    var jwtToken = getJwtToken();
    options = options || {};
    options.headers = options.headers || {};
    options.headers['Authorization'] = 'Bearer ' + jwtToken;
    var response = await fetch(url, options);
    var responseHeaders = response.headers.get('Token-Expired');
    if (response.ok) {
        //all is good, return the response
        return response;
    }

    if (response.status === 401 && response.headers.has('Token-Expired')) {
        var refreshToken = getRefreshToken();

        var refreshResponse = await refresh(jwtToken, refreshToken);
        if (!refreshResponse.ok) {
            return response; //failed to refresh so return original 401 response
        }
        var jsonRefreshResponse = await refreshResponse.json(); //read the json with the new tokens

        saveJwtToken(jsonRefreshResponse.token);
        saveRefreshToken(jsonRefreshResponse.refreshToken);
        return await fetchWithCredentials(url, options); //repeat the original request
    } else {
        //status is not 401 and/or there's no Token-Expired header
        return response; //return the original 401 response
    }
}

function getJwtToken() {
    return localStorage.getItem('acceccToken');
}

function getRefreshToken(): string {
    return localStorage.getItem('refreshToken') || '';
}

function saveJwtToken(token: any) {
    localStorage.setItem('acceccToken', token);
}

function saveRefreshToken(refreshToken: string) {
    localStorage.setItem('refreshToken', refreshToken);
}

async function refresh(jwtToken: any, refreshToken: any) {
    return fetch('http://localhost:49685/api/account/refresh', {
        method: 'POST',
        body: `token=${encodeURIComponent(jwtToken)}&refreshToken=${encodeURIComponent(getRefreshToken())}`,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });
}
