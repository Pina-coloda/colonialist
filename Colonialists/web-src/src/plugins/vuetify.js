import Vue from 'vue';
import Vuetify from 'vuetify/lib';
Vue.use(Vuetify);
export default new Vuetify({
    theme: {
        themes: {
            dark: {
                primary: '#9c27b0',
                secondary: '#999',
                warning: '#ff9800',
                error: '#f44336',
                success: '#4caf50',
                accent: '#000'
            }
        }
    }
});
//# sourceMappingURL=vuetify.js.map