module.exports = {
    root: true,
    env: {
        node: true
    },
    extends: ['plugin:vue/essential', '@vue/prettier', '@vue/typescript'],
    rules: {
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        "vue/max-attributes-per-line": ["error", {
            "singleline": 7,
            "multiline": {
                "max": 7,
                "allowFirstLine": true
            }
        }]
    },
    parserOptions: {
        parser: '@typescript-eslint/parser'
    }
};