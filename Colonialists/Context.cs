﻿using ColonialistsConsole;

namespace Colonialists
{
    public class Context
    {
        public Repository<Lobby> Lobby { get; set; } = new Repository<Lobby>();

        public Repository<Game> Games { get; set; } = new Repository<Game>();

        public Repository<User> Users { get; set; } = new Repository<User>();
    }
}