﻿using System;
using System.Linq;

namespace ColonialistsConsole
{
    /// <summary>
    /// Игрок
    /// </summary>
    public class Player

    {
        public Guid Id { get; set; } = Guid.NewGuid();

        private Game _game;

        /// <summary>
        /// Количество дорог
        /// </summary>
        public int RoadCount { get; set; } = 15;

        /// <summary>
        /// Количество поселений
        /// </summary>
        public int VillageCount { get; set; } = 5;

        /// <summary>
        /// Количество городов
        /// </summary>
        public int TownCount { get; set; } = 4;

        /// <summary>
        /// Количество дерева
        /// </summary>
        public int WoodCount { get; set; }

        /// <summary>
        /// Количество шерсти
        /// </summary>
        public int WoolCount { get; set; }

        /// <summary>
        /// Количество зерна
        /// </summary>
        public int GrainCount { get; set; }

        /// <summary>
        /// Количество глины
        /// </summary>
        public int ClayCount { get; set; }

        /// <summary>
        /// Количество руды
        /// </summary>
        public int OreCount { get; set; }

        /// <summary>
        /// Имя игрока
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Построить дорогу
        /// </summary>
        /// <param name="edge">Линия, на которой строят дорогу</param>
        public string BuildRoad(Edge edge)
        {
            if (RoadCount <= 0)
                return "Нехватает дорог";
            if (WoodCount <= 0 || ClayCount <= 0)
                return "Нехватает ресурсов";
            if (!edge.IsEmpty)
                return "Это место уже занято";
            if (!edge.Vertices.Any(x => x.Player == this) ||
                !_game.GameField.GetNeighboringEdges(edge).Any(x => x.Player == this))
                return "Вы не владеете этой дорогой, либо рядом нет ваших дорог";
            edge.BuildRoad(this);
            WoodCount--;
            ClayCount--;
            RoadCount--;
            return "Вы построили дорогу";
        }

        /// <summary>
        /// Построить поселение
        /// </summary>
        /// <param name="vertex">Вертекс, на котором строят поселение</param>
        public void BuildVillage(Vertex vertex)
        {
            if (VillageCount <= 0)
                return;
            if (WoodCount <= 0 || WoolCount <= 0 || ClayCount <= 0 || GrainCount <= 0)
                return;
            if (!vertex.IsEmpty)
                return;
            if (!_game.GameField.GetNeighboringEdges(vertex).Any(x => x.Player == this))
                return;
            if (_game.GameField.GetNeighboringVertices(vertex).Any(x => !x.IsEmpty))
                return;
            vertex.BuildVillage(this);
            WoodCount--;
            WoolCount--;
            ClayCount--;
            GrainCount--;
            VillageCount--;
        }

        /// <summary>
        /// Построить город
        /// </summary>
        /// <param name="vertex">Вертекс, на котором строят город</param>
        public void BuildTown(Vertex vertex)
        {
            if (TownCount <= 0)
                return;
            if (GrainCount <= 1 || OreCount <= 2)
                return;
            if (vertex.Player != this || !vertex.IsVillage)
                return;
            vertex.BuildTown(this);
            GrainCount -= 2;
            OreCount -= 3;
            TownCount--;
            VillageCount++;
        }

    }
}