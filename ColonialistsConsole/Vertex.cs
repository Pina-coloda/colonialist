﻿using System.Collections.Generic;

namespace ColonialistsConsole
{
    /// <summary>
    /// Вершина
    /// </summary>
    public class Vertex
    {
        /// <summary>
        /// Координаты
        /// </summary>
        public Coordinates Coordinates { get; private set; }

        /// <summary>
        /// Игрок, чье поселение тут построено
        /// </summary>
        private Player VillagePlayer { get; set; }

        /// <summary>
        /// Игрок, чей город тут построен
        /// </summary>
        private Player TownPlayer { get; set; }

        /// <summary>
        /// Список прилегающих гексов
        /// </summary>
        public List<Hex> Hexes { get;  } = new List<Hex>();

        /// <summary>
        /// Проверяет не построено ли что-то
        /// </summary>
        public bool IsEmpty => VillagePlayer == null && TownPlayer == null;

        /// <summary>
        /// Если построено, то чье
        /// </summary>
        public Player Player => VillagePlayer ?? TownPlayer;

        /// <summary>
        /// Построено ли поселение
        /// </summary>
        public bool IsVillage => VillagePlayer != null;

        /// <summary>
        /// Построен ли город
        /// </summary>
        public bool IsTown => TownPlayer != null;


        /// <summary>
        /// Постройка поселения
        /// </summary>
        /// <param name="player">Игрок, который строит поселение</param>
        public void BuildVillage(Player player)
        {
            if(!IsEmpty)
                return;
            VillagePlayer = player;
        }

        /// <summary>
        /// Постройка города
        /// </summary>
        /// <param name="player">Игрок, который строит город</param>
        public void BuildTown(Player player)
        {
            if (VillagePlayer == player)
            {
                VillagePlayer = null;
                TownPlayer = player;
            }
        }

        public  bool Equals(Vertex vertex)
        {
            return Coordinates.Equals(vertex.Coordinates);
        }

        public Vertex(Coordinates coordinates)
        {
            Coordinates = coordinates;
        }
    }
}