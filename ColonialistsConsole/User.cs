﻿using System;

namespace ColonialistsConsole
{
    public class User : IModel
    {
        public Guid Id { get; set; } = Guid.NewGuid();

        public string Name { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public string RefreshToken { get; set; }

        public bool Equals(User user)
        {
            if (user != null)
                return Id == user.Id;
            return false;
        }
        public override bool Equals(Object obj)
        {
            if (obj is User user)
            {
               return Equals(user);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

    }
}