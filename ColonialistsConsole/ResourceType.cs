﻿namespace ColonialistsConsole
{
    /// <summary>
    /// Типы ресурсов
    /// </summary>
    public enum ResourceType
    {
        /// <summary>
        /// Дерево 0
        /// </summary>
        Wood, 
        /// <summary>
        /// Шерсть 1
        /// </summary>
        Wool,
        /// <summary>
        /// Зерно 2
        /// </summary>
        Grain,
        /// <summary>
        /// Глина 3
        /// </summary>
        Clay,
        /// <summary>
        /// Руда 4
        /// </summary>
        Ore
    }
}