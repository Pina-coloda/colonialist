﻿using System;

namespace ColonialistsConsole
{
    public class Coordinates
    {
        /// <summary>
        /// Радиус
        /// </summary>
        public double Radius { get; private set; }

        /// <summary>
        /// Угол
        /// </summary>
        public double Angle { get; private set; }

        public double Distance(Coordinates coordinates)
        {
            return Math.Sqrt(Radius * Radius + coordinates.Radius * coordinates.Radius -
                             2 * Radius * coordinates.Radius * Math.Cos((Angle - coordinates.Angle) * Math.PI / 180));
        }

        public  bool Equals(Coordinates coordinates)
        {
            return Math.Abs(Radius - coordinates.Radius) < 1e6 && Math.Abs(Angle - coordinates.Angle) < 1e6;
        }

        public Coordinates(double radius, double angle)
        {
            Radius = radius;
            Angle = angle;
        }
    }
}