﻿using System;
using System.Collections.Generic;
using System.Linq;
using static System.Math;

namespace ColonialistsConsole
{
    public class GameField
    {
        public List<Hex> Hexes { get; private set; }
        public List<Vertex> Vertices = new List<Vertex>();
        public List<Edge> Edges = new List<Edge>();
        
        public GameField()
        {
            var r = new Random();
            var numbers = new[] {2, 3, 3, 4, 4, 5, 5, 6, 6, 8, 8, 9, 9, 10, 10, 11, 11, 12}.OrderBy(x => r.Next()).ToList();
            numbers.Add(7);
            var resourceTypes = new ResourceType?[]
            {
                ResourceType.Wood, ResourceType.Wood, ResourceType.Wood, ResourceType.Wood, ResourceType.Wool,
                ResourceType.Wool, ResourceType.Wool, ResourceType.Wool, ResourceType.Clay, ResourceType.Clay,
                ResourceType.Clay, ResourceType.Ore, ResourceType.Ore, ResourceType.Ore, ResourceType.Grain,
                ResourceType.Grain, ResourceType.Grain, ResourceType.Grain
            }.OrderBy(x => r.Next()).ToList();
            resourceTypes.Add(null);
            var coordinates = new List<Coordinates>();
            coordinates.Add(new Coordinates(0, 0));
            coordinates.AddRange(Enumerable.Repeat(Sqrt(3), 6).Select((x, i) => new Coordinates(x, 30 + 60 * i)));
            coordinates.AddRange(Enumerable.Repeat(3, 6).Select((x, i) => new Coordinates(x, 60 * i)));
            coordinates.AddRange(Enumerable.Repeat(2*Sqrt(3), 6).Select((x, i) => new Coordinates(x, 30 + 60 * i)));
            coordinates = coordinates.OrderBy(x => r.Next()).ToList();
            Hexes = numbers.Zip(resourceTypes, (number, resourceType) => (number, resourceType))
                .Zip(coordinates, (x, coord) => (x.number, x.resourceType, coord))
                .Select(x => new Hex(x.resourceType, x.number, x.coord)).ToList();

            for (int i = 0; i < 6; i++)
            {
               Vertices.Add(new Vertex(new Coordinates(1, 60*i))); 
               Vertices.Add(new Vertex(new Coordinates(2, 60*i))); 
               Vertices.Add(new Vertex(new Coordinates(4, 60*i))); 
            }

            Vertices.AddRange(CreateVertices(Sqrt(7), new []{1, 2, 3}));
            Vertices.AddRange(CreateVertices(Sqrt(13), new []{1, 3, 4}));
            Vertices.AddRange(CreateVertices(Sqrt(19), new []{2, 3, 5}));

            foreach (var vertex in Vertices)
            {
                vertex.Hexes.AddRange(Hexes.Where(x => Abs(1 - vertex.Coordinates.Distance(x.Coordinates)) < 1e-6));
            }

            foreach (var vertex in Vertices)
            {
                var vertices2 = Vertices.Where(x => Abs(1 - vertex.Coordinates.Distance(x.Coordinates)) < 1e-6).ToList();
                foreach (var vertex2 in vertices2)
                {
                    if (Edges.Any(x => x.Vertices.Contains(vertex) && x.Vertices.Contains(vertex2)))
                        continue;
                    Edges.Add(new Edge(vertex, vertex2));
                }
            }
        }

        private static List<Vertex> CreateVertices(double r, int[] j)
        {
            var result = new List<Vertex>();

            for (int i = 0; i < j.Length; i++)
            {
                result.Add(new Vertex(new Coordinates(r, Asin(Sqrt(3)/r*j[i]/2)*180/PI)));
                result.Add(new Vertex(new Coordinates(r, 360-Asin(Sqrt(3)/r*j[i]/2) * 180 / PI)));
                result.Add(new Vertex(new Coordinates(r, 180-Asin(Sqrt(3)/r*j[i]/2) * 180 / PI)));
                result.Add(new Vertex(new Coordinates(r, 180+Asin(Sqrt(3)/r*j[i]/2) * 180 / PI)));
            }

            return result;
        }

        public List<Edge> GetNeighboringEdges(Edge edge)
        {
            return (from vertex in edge.Vertices
                    from edge1 in Edges
                    where edge1 != edge && edge1.Vertices.Contains(vertex)
                    select edge1)
                 .ToList();
        }

        public List<Edge> GetNeighboringEdges(Vertex vertex)
        {
            return (from edge1 in Edges
                    where edge1.Vertices.Contains(vertex)
                    select edge1)
                .ToList();
        }

        public List<Vertex> GetNeighboringVertices(Vertex vertex)
        {
            return Vertices.Where(x => Abs(1 - vertex.Coordinates.Distance(x.Coordinates)) < 1e-6).ToList();
        }
    }
}