﻿using System;
using System.Collections.Generic;
using System.Linq;
using LanguageExt;

namespace ColonialistsConsole
{
    public class Lobby : IModel
    {
        /// <summary>
        /// Идентификатор лобби
        /// </summary>
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Список пользователей в лобби
        /// </summary>
        public readonly ICollection<User> Users = new List<User>();

        /// <summary>
        /// Название лобби
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Добавление пользователя
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool AddUser(User user)
        {
            if (Users.Count < 4)
            {
                Users.Add(user);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Удаление пользователя
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool DeleteUser(User user)
        {
            return  Users.Remove(user);

        }

        /// <summary>
        /// Удаление пользователя
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool DeleteUser(Guid id)
        {
            return Users.Remove(new User{Id =  id});
        }

        /// <summary>
        /// Создание игры
        /// </summary>
        /// <returns></returns>
        public Option<Game> CreateGame()
        {
            return Game.CreateGame(Users);
        }

        public bool Equals(Lobby lobby)
        {
            if (lobby != null)
                return Id == lobby.Id;
            return false;
        }
        public override bool Equals(Object obj)
        {
            if (obj is Lobby lobby)
            {
                return Equals(lobby);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}