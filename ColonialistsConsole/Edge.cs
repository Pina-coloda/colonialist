﻿namespace ColonialistsConsole
{
    /// <summary>
    /// Линии
    /// </summary>
    public class Edge
    {
        /// <summary>
        /// Массив Вертексов
        /// </summary>
        public readonly Vertex[] Vertices = new Vertex[2];

        /// <summary>
        /// Игрок, у которого здесь построена дорога
        /// </summary>
        public Player Player { get; private set; }

        /// <summary>
        /// Пустая ли линия
        /// </summary>
        public bool IsEmpty => Player == null;


        /// <summary>
        /// Построить дорогу
        /// </summary>
        /// <param name="player">Игрок, котрый строит дорогу</param>
        public void BuildRoad(Player player)
        {
            if (!IsEmpty)
                return;
            Player = player;
        }

        public bool Equals(Edge edge)
        {
            return (Vertices[0].Equals(edge.Vertices[0]) && Vertices[1].Equals(edge.Vertices[1])) || (Vertices[0].Equals(edge.Vertices[1]) && Vertices[1].Equals(edge.Vertices[0]));
        }

        public Edge(Vertex vertex1, Vertex vertex2)
        {
            Vertices[0] = vertex1;
            Vertices[1] = vertex2;
        }
    }
}