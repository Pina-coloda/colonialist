﻿using System;

namespace ColonialistsConsole
{
    public interface IModel
    {
        Guid Id { get; set; }
    }
}