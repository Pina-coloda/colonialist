﻿namespace ColonialistsConsole
{
    /// <summary>
    /// Гекс
    /// </summary>
    public class Hex
    {
        /// <summary>
        /// Тип ресурса
        /// </summary>
        public ResourceType? ResourceType { get; private set; } 

        /// <summary>
        /// Номер
        /// </summary>
        public int Number { get; private set; }
        
        /// <summary>
        /// Координаты центра
        /// </summary>
        public Coordinates Coordinates { get; private set; }

        public Hex(ResourceType? resourceType, int number, Coordinates coordinates)
        {
            ResourceType = resourceType;
            Number = number;
            Coordinates = coordinates;
        }

        public override string ToString()
        {
            return $"{ResourceType} {Number}";
        }
    }
}