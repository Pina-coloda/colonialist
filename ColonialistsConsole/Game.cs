﻿using System;
using System.Collections.Generic;
using System.Linq;
using LanguageExt;

namespace ColonialistsConsole
{
    public class Game : IModel
    {
        /// <summary>
        /// Идентификатор игры
        /// </summary>
        public Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Создание игры
        /// </summary>
        /// <param name="players"></param>
        /// <returns></returns>
        public static Option<Game> CreateGame(ICollection<User> users)
        {
            if (users == null)
                return Option<Game>.None;
            if (users.Count < 2 || users.Count > 4)
                return Option<Game>.None;
            var players = users.Select(x => new Player{Name = x.Name}).ToList();
            return new Game(players);
        }

        /// <summary>
        /// Игровое поле
        /// </summary>
        public GameField GameField;

        /// <summary>
        /// Список игроков
        /// </summary>
        public List<Player> Players;

        public bool Equals(Game game)
        {
           
            if (game != null)
                return Id == game.Id;
            return false;
        }
        public override bool Equals(Object obj)
        {
            if (obj is Game game)
            {
                return Equals(game);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        private Game(List<Player> players)
        {
            Players = players;
            GameField = new GameField();
        }
    }
}